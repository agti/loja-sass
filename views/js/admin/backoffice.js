document.addEventListener('DOMContentLoaded', function(){
	//parâmetros avançados->clientes
	$('#form_general_enable_b2b_mode_0').closest('.form-group').remove();
	$('#form_general_ask_for_birthday_0').closest('.form-group').remove();	

	//parâmetros da loja -> Preferências
	$('.adminpreferences .form-group:first-child').remove();
	$('#form_general_enable_ssl_everywhere_0').closest('.form-group').remove();
	$('#form_general_enable_token_0').closest('.form-group').remove();
	$('#form_general_allow_html_iframes_0').closest('.form-group').remove();
	$('#form_general_use_htmlpurifier_1').closest('.form-group').remove();
	$('#form_general_price_round_mode').closest('.form-group').remove();
	$('#form_general_price_round_type').closest('.form-group').remove();
	$('#form_general_multishop_feature_active_0').closest('.form-group').remove();
	$('#form_general_shop_activity').closest('.form-group').remove();
});