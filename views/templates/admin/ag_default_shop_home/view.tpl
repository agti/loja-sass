<h2>Bem-vindo à sua loja AGTI!</h2>

<p>
    Aqui você tem acesso às principais configurações de sua loja, além de tutoriais para te ajudar a iniciar as vendas!
</p>

<h3  class="vertical-middle"><i class="material-icons">local_shipping</i> Frete</h3>

<ul>
    <li>Se você deseja utilizar os serviços dos Correios, Jadlog ou Jamef, recomendamos o módulo <a href="{$link->getAdminLink('AdminModules')}&configure=agmelhorenvio">Melhor Envio</a>.</li>
    <li>Se trabalha com pacotes muito grandes ou pesados, recomendamos o módulo <a href="{$link->getAdminLink('AdminModules')}&configure=agcargobr">CargoBR</a>.</li>
</ul>

<h3 class="vertical-middle"><i class="material-icons">payment</i> Pagamentos</h3>
<ul>
    <li>Configure as suas opções de recebimento via Boleto Bancário e Cartão de Crédito através do <a href="{$link->getAdminLink('AdminModules')}&configure=agmoipmarketplace">MOIP</a>.</li>
    <li>Cadastre <a href="{$link->getAdminLink('AdminModules')}&configure=agpaymentmodes">Formas de pagamento Personalizadas</a>, como cheque, cartão BNDES, dinheiro, etc.</li>
    <li>Configure a exibição das <a href="{$link->getAdminLink('AdminModules')}&configure=agpaymentsimulator">simulações de pagamento</a> à vista e parcelado.</li>
</ul>
<h3 class="vertical-middle"><i class="material-icons">grade</i> Personalizações e Layout</h3>
<ul>
    <li><a href="{$link->getAdminLink('AdminModules')}&configure=ps_banner">Banner da Página Inicial</a></li>
    <li><a href="{$link->getAdminLink('AdminThemes')}">Logo</a></li>
    <li><a href="{$link->getAdminLink('AdminModules')}&configure=blockreassurance">Políticas de Devolução e Garantia</a></li>
    <li><a href="{$link->getAdminLink('AdminModules')}&configure=ps_customtext">Texto da Página Inicial</a></li>
    <li><a href="{$link->getAdminLink('AdminCmsContent')}">Termos de Uso e Páginas CMS</a></li>
    <li><a href="{$link->getAdminLink('AdminContacts')}">Informações de Contato</a></li>
    <li><a href="{$link->getAdminLink('AdminModules')}&configure=ps_mainmenu">Menus da Loja</a></li>
    <li><a href="{$link->getAdminLink('AdminModules')}&configure=agdefaulttheme">Cores da Loja</a></li>
</ul>

<h3 class="vertical-middle"><i class="material-icons">help</i> Tutoriais</h3>
    <ul>
        <li><a href="https://www.agti.eng.br/br/blog/tutoriais/cadastrando-produtos-na-versao-17-da-plataforma-prestashop" target="_blank">Cadastrar Produtos</a></li>
        <li>Em breve mais tutoriais.</li>
        {* <li>Combinações de Produtos</li>
        <li>Árvore de Categorias</li>
        <li>SEO</li> *}
    </ul>
