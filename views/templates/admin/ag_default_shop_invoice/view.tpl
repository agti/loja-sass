<div class="panel col-lg-12">
    <h3>Fatura #{$invoice->invoice->id_aggerenciador_invoice}</h3>

    <div class="panel">
        <div class="panel-heading">
            Informações Gerais
        </div>

        <dl class="dl-horizontal">
            <dt>Estado</dt>
            <dd>{AgDefaultShop::$invoice_status[$invoice->invoice->status]}</dd>

            <dt>Data de Fechamento</dt>
            <dd>{Tools::displayDate($invoice->invoice->date_commit)}</dd>

            <dt>Data de Vencimento</dt>
            <dd>{Tools::displayDate($invoice->invoice->date_expiration)}</dd>

            <dt>Valor</dt>
            <dd>{Tools::displayPrice($invoice->invoice->total)}</dd>
        </dl>

        {if ($invoice->invoice->status == 1 || $invoice->invoice->status == 2) && $invoice->print_ticket_link|default}
            <a class="btn btn-primary" href="{$invoice->print_ticket_link}" target="_blank">
                <i class="icon-barcode"></i> Imprimir Boleto
            </a>
        {/if}
    </div>
    


        <div class="table-responsive-row clearfix">
            <table class="table configuration">
                <thead>
                    <tr class="nodrag nodrop">
                        <th>
                            <span class="title-box">Data</span>
                        <th>
                            <span class="title-box">Descrição</span>
                        <th class="center fixed-width-sm">
                            <span class="title-box">Quantidade</span>
                        <th class="center fixed-width-sm">
                            <span class="title-box">Preço</span>
                        <th class="center fixed-width-sm">
                            <span class="title-box">Desconto</span>
                        <th class="center fixed-width-sm">
                            <span class="title-box">Total</span>
                    </tr>
                </thead>
                <tbody>
                    {foreach from=$invoice->items item=item key=i}
                        <tr {if i % 2}class="odd"{/if} data-id="{$item->id_aggerenciador_invoice_item}">
                        <td class="center fixed-width-md">{Tools::displayDate($item->date_add)}
                        <td>{$item->description}
                        <td class="center fixed-width-sm">{$item->quantity}
                        <td class="center fixed-width-sm">{Tools::displayPrice($item->price)}
                        <td class="center fixed-width-sm">{Tools::displayPrice($item->discounts)}
                        <td class="center fixed-width-sm">{Tools::displayPrice($item->price_total)}
                    {/foreach}
                </tbody>
            </table>
        </div>
    </div>
</div>