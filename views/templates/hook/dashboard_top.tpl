<div id='agdefaultshop_dashboard_top' class="row">
	<div class="panel">
	    <div class="panel-heading">AgShop - Plano II</div>
	    
	    <div>
	    	{foreach $alerts.errors as $error}
		    	<div class="alert alert-danger">
					{$error}
				</div>
	    	{/foreach}

	    	{foreach $alerts.warnings as $warning}
		    	<div class="alert alert-warning">
					{$warnings}
				</div>
	    	{/foreach}

	    	{foreach $alerts.success as $success}
		    	<div class="alert alert-success">
					{$success}
				</div>
	    	{/foreach}

	    	{foreach $alerts.infos as $info}
		    	<div class="alert alert-info">
					{$info}
				</div>
	    	{/foreach}
		</div>

		<div class="row">
			<div class='well'>
				{include file=$modules_dir|cat:'agcliente/views/templates/hook/includes/progress_bar.tpl' width=$qty_products * 100 / $plan_data.max_products}
				<p>{$qty_products} produtos de {$plan_data.max_products} disponíveis ({$qty_products * 100 / $plan_data.max_products}%)</p>
				<p>Necessita de mais produtos? <a href="{$agdefaultshop_link}">Entre em contato</a> e peça a alteração 
				de seu plano!</p>
			</div>

			<div class="">
				<a class='btn btn-default' href='{$wiki_link}' target="_blank">Tutoriais de Ajuda</a></li>
				<a class='btn btn-default' href='{$agdefaultshop_link}'>Formulário de Suporte</a></li>
			</div>
		</div>
	</div>
</div>