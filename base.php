<?php

require_once _PS_MODULE_DIR_ . 'agcliente/lib/AgModule.php';
class BaseAgDefaultShop extends AgModule
{
    public $hooks = array(
        'actionAdminLoginControllerSetMedia',
        'displayBackOfficeHeader',
        'dashboardZoneOne',
        'actionObjectProductAddBefore'
    );

    public $main_tab = '';

    protected $tabs = [
        [
            "name"      => "Faturas",
            "className" => "AdminAgDefaultShopInvoice",
            "active"    => 1
        ],
        [
            "name"      => "Página Inicial",
            "className" => "AdminAgDefaultShopHome",
            "active"    => 1
        ],
    ];

    public static $invoice_status = [
        '0' => 'Em Aberto',
        '1' => 'Aguardando Pagamento',
        '2' => 'Paga',
        '3' => 'Vencida'
    ];

    public function __construct()
    {
        
        $this->name = 'agdefaultshop';
        $this->version                = '1.0.3';
        $this->bootstrap              = true;
        $this->author                 = 'AGTI';
        $this->need_instance          = 1;
        $this->ps_versions_compliancy = array('min' => '1.7', 'max' => _PS_VERSION_);

        parent::__construct();
        
        $this->displayName = $this->l('AGTI Shop');
        $this->description = $this->l('Create Standardized shops for AGTI');
    }

    public function getContent()
    {
        agcliente::prepareConfigHelpTab($this->name);

        $this->context->smarty->assign([
            'has_support'        => true,

            'modules_path' => _PS_MODULE_DIR_
        ]);

        return $this->display(_PS_MODULE_DIR_ . $this->name, 'views/templates/admin/configuration.tpl');
    }

    /******************** funções auxiliares ***********************/

    public function copyTo($from, $to)
    {
        $success = copy($from, $to);
        if (!$success) {
            throw new Exception("Erro copiando arquivo de $from para $to.");
        }
    }

    public function getProfileByName($name)
    {
        $sql = new DbQuery();
        $sql->from('profile_lang');
        $sql->where('name="' . pSQL($name) .'"');
        
        $db_data = Db::getInstance()->getRow($sql);
        return $db_data;
    }


    public function getPlanData()
    {
        $data = unserialize(Configuration::get('agcliente_plan_data'));
        return $data;
    }

    public function getQtyProducts()
    {
        $sql = new DbQuery;
        $sql->select('count(*)')
            ->from('product_shop')
            ->where('id_shop=' . (int)$this->context->shop->id);

        $qty_products = Db::getInstance()->getValue($sql);
        return $qty_products;
    }

    public function checkProductQuantities()
    {
        $qty_products = $this->getQtyProducts();
        $plan_data = $this->getPlanData();

        if ($qty_products > $plan_data['max_products']  ) {
            Configuration::updateValue('PS_SHOP_ENABLE', 0);
            Configuration::updateValue('MAINTENANCE_BECAUSE_PRODUCTS_QTY', 1);

            return false;
        } elseif(Configuration::GET('MAINTENANCE_BECAUSE_PRODUCTS_QTY', 1)) {
            Configuration::updateValue('PS_SHOP_ENABLE', 1);
        }

        return true;
    }

    /********************** HOOKS ************************/
    public function hookDisplayBackOfficeHeader()
    {
        $this->context->controller->addCss($this->_path . 'views/css/base.css');

        if (Tools::getValue('controller') === 'AdminDashboard') {
            $this->context->controller->addJs($this->_path . 'views/js/admin/dashboard.js');

            if (Tools::getIsSet('load_agdefaultshop_top')) {
                echo $this->renderDashboardTop();
                exit();
            }
        }

        if ($this->context->controller->controller_name === 'AdminProducts') {
            if (!$this->checkProductQuantities()) {
                $this->context->controller->warnings[] = 'Você já atingiu o máximo de produtos disponível para o seu plano.';
            }
            $this->context->controller->addJs($this->_path . 'views/js/admin/products.js');
        }

        $plan_data = $this->getPlanData();
        $this->context->smarty->assign([
            'max_products' => $plan_data['max_products'],
            'qty_products' => $this->getQtyProducts(),
            'plan_name'    => $plan_data['plan_name']
        ]);

        $this->context->controller->addJs($this->_path . 'views/js/admin/backoffice.js');
        return $this->display($this->_path, 'back_office_header.tpl');
    }

    public function hookActionAdminLoginControllerSetMedia()
    {
        $this->context->controller->addCss($this->_path . 'views/css/admin_login.css');
    }

    public function hookDashboardZoneOne()
    {
        $sql = new DbQuery;
        $sql->select('count(*)')
            ->from('product_shop')
            ->where('id_shop=' . (int)$this->context->shop->id);

        $qty_products = Db::getInstance()->getValue($sql);
        $plan_data = $this->getPlanData();
        $remote = AgClienteConfig::getRemote($this->name);

        $errors = [];
        $warnigs = [];
        $success = [];
        $infos = [];

        if (!$this->checkProductQuantities()) {
            $errors[] = 'A sua loja foi desativada temporariamente porque você possui mais produtos do que o contratado.';
        }

        $this->context->smarty->assign([
            'qty_products' => $qty_products,
            'plan_data' => $plan_data,
            'agdefaultshop_link' => $this->context->link->getAdminLink('AdminModules') . '&configure=agdefaultshop',
            'wiki_link' => $remote->wiki_url,
            'alerts' => [
                'errors'  => $errors,
                'warnigs' => $warnigs,
                'success' => $success,
                'infos'   => $infos
            ],
            'modules_dir' => _PS_MODULE_DIR_
        ]);

        return $this->display($this->_path, 'dashboard_top.tpl');
    }

    public function hookActionObjectProductAddBefore()
    {
        if (!$this->checkProductQuantities()) {
            throw new PrestaShopException('Você já possui o máximo de produtos disponíveis para a sua loja.');
        }
    }
}