<?php

class AgDefaultShopConfirmPlanInfoReceivedModuleFrontController extends ModuleFrontController
{
    public function initContent()
    {
        $token = Tools::getValue('token');

        if ($token != Configuration::get('agcliente_token')) {
            AgCommunicator::checkToken($token);
        }

        $info = Tools::getValue('info');
        if (isset($info['orders']) && is_array($info['orders'])) {
            //id dos pedidos a serem marcados como 'enviados'
            $ids = array();
            foreach ($info['orders'] as $order) {
                $ids[] = $order['id_order'];
            }
            
            Db::getInstance()->update(
                'orders',
                array('sent_to_agti' => 1),
                'id_order IN (' . implode(',', $ids) . ')'
            );
        }

        echo Tools::jsonEncode(array(
            'success' => 1
        ));

        exit();
    }
}
