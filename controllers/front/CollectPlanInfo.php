<?php

class AgDefaultShopCollectPlanInfoModuleFrontController extends ModuleFrontController
{
    public function initContent()
    {
        $token = Tools::getValue('token');
        try {
            if ($token != Configuration::get('agcliente_token')) {
                AgCommunicator::checkToken($token);
            }

            //obtém pedidos
            $sql = new DbQuery();
            $sql->select('id_order');
            $sql->from('orders');
            $sql->where('sent_to_agti=0');

            $orders = Db::getInstance()->executeS($sql);

            echo Tools::jsonEncode([
                'success' => true,
                'response' => [
                    'orders' => $orders
                ]
            ]);
        } catch (Exception $e) {
            echo Tools::jsonEncode([
                'success' => false,
                'error_msg' => 'Erro na verificação do token'
            ]);
        }

        exit();
    }
}
