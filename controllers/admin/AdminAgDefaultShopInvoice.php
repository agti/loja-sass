<?php

class AdminAgDefaultShopInvoiceController extends ModuleAdminController
{
    public function __construct()
    {
        $this->bootstrap = true;
        $this->list_no_link = true;

        parent::__construct();
        
        $this->fields_list = [
            'id_aggerenciador_invoice' => [
                'title' => 'ID',
                'type' => 'int'
            ],
            'total' => [
                'title' => 'Total',
                'type' => 'price'
            ],
            'date_commit' => [
                'title' => 'Data de Fechamento',
                'type' => 'datetime'
            ],
            'date_expiration' => [
                'title' => 'Data de Vencimento',
                'type' => 'datetime'
            ],
            'status' => [
                'title' => 'Status',
                'type' => 'text'
            ]
        ];

        if (!Tools::getIsSet('id_aggerenciador_invoice')) {
            $token = Configuration::get('agcliente_token');
            $this->invoices = AgCommunicator::getInvoices($token);
        }
    }

    public function initContent()
    {
        parent::initContent();

        if (!$this->display) {
            $this->setTemplate('invoices.tpl');

            $this->context->smarty->assign([
                'invoices' =>  $this->invoices,
                'list' => $this->renderList()
            ]);
        } elseif ($this->display === 'view') {
            $id_invoice = Tools::getValue('id_aggerenciador_invoice');
            $token = Configuration::get('agcliente_token');

            $invoice = AgCommunicator::getInvoice($id_invoice, $token);
            $this->setTemplate('view.tpl');

            $this->context->smarty->assign([
                'invoice' =>  $invoice
            ]);
        }
    }

    public function renderList()
    {
        $hl = new HelperList();

        $hl->shopLinkType = "";
        $hl->title = 'Faturas Recentes';
        $hl->token = Tools::getAdminTokenLite(get_class($this));

        $link = new LinkCore();
        $hl->currentIndex = self::$currentIndex;
        $hl->identifier = 'id_aggerenciador_invoice';
        $hl->simple_header = true;
        $hl->show_toolbar = false;
        $hl->token = $this->token;

        $hl->listTotal = count($this->invoices);
        $hl->actions = ['view'];

        $invoices_array = [];
        foreach ($this->invoices as $invoice) {
            $invoice_array = (array)$invoice;
            $invoice_array['status'] = AgDefaultShop::$invoice_status[$invoice->status];
            $invoices_array[] = $invoice_array;
        }

        return $hl->generateList($invoices_array, $this->fields_list);
    }
}
