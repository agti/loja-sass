<?php
class AdminAgDefaultShopHomeController extends ModuleAdminController
{
    public function initContent()
    {
        parent::initContent();

        $this->setTemplate('view.tpl');
    }

    public function setMedia()
    {
        parent::setMedia();

        $this->addCss([
            _PS_MODULE_DIR_ . $this->module->name . '/views/css/admin_home.css'
        ]);
    }
}
